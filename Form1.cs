﻿using System.Drawing;
using System.Windows.Forms;
using TongueClassLibrary;

namespace ai_tongue_window_demo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            InitClient();
        }

        private AITongueClient aiTongueClient;

        private void InitClient()
        {
            aiTongueClient = new AITongueClient();
            aiTongueClient.BackColor = Color.White;
            aiTongueClient.Dock = DockStyle.Fill;
            aiTongueClient.Name = "aiTongueClient";

            // 检测流程完成后触发
            aiTongueClient.onCompletedEvent += new AITongueClient.OnCompletedEvent((string pdfPath, string jsonPath) =>
            {
                this.lb_pdf_file_path.Text = pdfPath;
                this.lb_json_file_path.Text = jsonPath;
            });
            // 点击退出按钮时触发
            //aiTongueClient.onClickQuitEvent += new AITongueClient.OnClickQuitEvent(() =>
            //{
            //});
            groupBox1.Controls.Add(aiTongueClient);

            Configs configs = new Configs();
            configs.corpCode = CorpInfo.corpCode;
            configs.publicKey = CorpInfo.publicKey;
            configs.privateKey = CorpInfo.privateKey;
            aiTongueClient.Init(configs);
        }

        private void btn_start_Click(object sender, System.EventArgs e)
        {
            PatientInfo patientInfo = new PatientInfo();
            patientInfo.patientName = this.tb_patient_name.Text.ToString().Trim();

            int sex = 0;
            if (this.rb_patient_man.Checked)
            {
                sex = 1;
            }
            else if (this.rb_patient_woman.Checked)
            {
                sex = 2;
            }
            patientInfo.patientSex = sex;

            patientInfo.patientAge = (int)this.nd_patient_age.Value;

            aiTongueClient.start(patientInfo);

            this.lb_pdf_file_path.Text = "";
            this.lb_json_file_path.Text = "";
        }
    }
}

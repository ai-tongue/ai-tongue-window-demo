﻿namespace ai_tongue_window_demo
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tb_disease_code = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lb_json_file_path = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lb_pdf_file_path = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btn_start = new System.Windows.Forms.Button();
            this.nd_patient_age = new System.Windows.Forms.NumericUpDown();
            this.rb_patient_woman = new System.Windows.Forms.RadioButton();
            this.rb_patient_man = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_patient_name = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nd_patient_age)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(869, 876);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.AutoSize = true;
            this.panel1.Controls.Add(this.tb_disease_code);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.lb_json_file_path);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.lb_pdf_file_path);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.btn_start);
            this.panel1.Controls.Add(this.nd_patient_age);
            this.panel1.Controls.Add(this.rb_patient_woman);
            this.panel1.Controls.Add(this.rb_patient_man);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.tb_patient_name);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(891, 12);
            this.panel1.Name = "panel1";
            this.panel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.panel1.Size = new System.Drawing.Size(342, 876);
            this.panel1.TabIndex = 1;
            // 
            // tb_disease_code
            // 
            this.tb_disease_code.Location = new System.Drawing.Point(38, 224);
            this.tb_disease_code.Name = "tb_disease_code";
            this.tb_disease_code.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tb_disease_code.Size = new System.Drawing.Size(220, 21);
            this.tb_disease_code.TabIndex = 15;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(36, 202);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label5.Size = new System.Drawing.Size(89, 12);
            this.label5.TabIndex = 14;
            this.label5.Text = "疾病类别编码：";
            // 
            // lb_json_file_path
            // 
            this.lb_json_file_path.AutoSize = true;
            this.lb_json_file_path.Location = new System.Drawing.Point(114, 327);
            this.lb_json_file_path.Name = "lb_json_file_path";
            this.lb_json_file_path.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lb_json_file_path.Size = new System.Drawing.Size(0, 12);
            this.lb_json_file_path.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(30, 327);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label6.Size = new System.Drawing.Size(89, 12);
            this.label6.TabIndex = 12;
            this.label6.Text = "json文件地址：";
            // 
            // lb_pdf_file_path
            // 
            this.lb_pdf_file_path.AutoSize = true;
            this.lb_pdf_file_path.Location = new System.Drawing.Point(114, 299);
            this.lb_pdf_file_path.Name = "lb_pdf_file_path";
            this.lb_pdf_file_path.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lb_pdf_file_path.Size = new System.Drawing.Size(0, 12);
            this.lb_pdf_file_path.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(36, 299);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label4.Size = new System.Drawing.Size(83, 12);
            this.label4.TabIndex = 10;
            this.label4.Text = "pdf文件地址：";
            // 
            // btn_start
            // 
            this.btn_start.Location = new System.Drawing.Point(36, 261);
            this.btn_start.Name = "btn_start";
            this.btn_start.Size = new System.Drawing.Size(75, 23);
            this.btn_start.TabIndex = 9;
            this.btn_start.Text = "开始";
            this.btn_start.UseVisualStyleBackColor = true;
            this.btn_start.Click += new System.EventHandler(this.btn_start_Click);
            // 
            // nd_patient_age
            // 
            this.nd_patient_age.Location = new System.Drawing.Point(38, 169);
            this.nd_patient_age.Name = "nd_patient_age";
            this.nd_patient_age.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.nd_patient_age.Size = new System.Drawing.Size(120, 21);
            this.nd_patient_age.TabIndex = 8;
            // 
            // rb_patient_woman
            // 
            this.rb_patient_woman.AutoSize = true;
            this.rb_patient_woman.Location = new System.Drawing.Point(91, 118);
            this.rb_patient_woman.Name = "rb_patient_woman";
            this.rb_patient_woman.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rb_patient_woman.Size = new System.Drawing.Size(35, 16);
            this.rb_patient_woman.TabIndex = 7;
            this.rb_patient_woman.TabStop = true;
            this.rb_patient_woman.Text = "女";
            this.rb_patient_woman.UseVisualStyleBackColor = true;
            // 
            // rb_patient_man
            // 
            this.rb_patient_man.AutoSize = true;
            this.rb_patient_man.Location = new System.Drawing.Point(36, 118);
            this.rb_patient_man.Name = "rb_patient_man";
            this.rb_patient_man.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rb_patient_man.Size = new System.Drawing.Size(35, 16);
            this.rb_patient_man.TabIndex = 6;
            this.rb_patient_man.TabStop = true;
            this.rb_patient_man.Text = "男";
            this.rb_patient_man.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(36, 146);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "年龄：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 94);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "性别：";
            // 
            // tb_patient_name
            // 
            this.tb_patient_name.Location = new System.Drawing.Point(36, 58);
            this.tb_patient_name.Name = "tb_patient_name";
            this.tb_patient_name.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tb_patient_name.Size = new System.Drawing.Size(220, 21);
            this.tb_patient_name.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 38);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "姓名：";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1245, 900);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "云诊科技AI舌诊控件演示";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nd_patient_age)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lb_json_file_path;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lb_pdf_file_path;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_start;
        private System.Windows.Forms.NumericUpDown nd_patient_age;
        private System.Windows.Forms.RadioButton rb_patient_woman;
        private System.Windows.Forms.RadioButton rb_patient_man;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_patient_name;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tb_disease_code;
    }
}


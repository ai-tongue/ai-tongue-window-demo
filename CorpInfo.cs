﻿namespace ai_tongue_window_demo
{
    public class CorpInfo
    {
        /// <summary>
        /// 企业编码
        /// </summary>
        public readonly static string corpCode = "test";
        /// <summary>
        /// 公钥
        /// </summary>
        public readonly static string publicKey = "test";
        /// <summary>
        /// 私钥
        /// </summary>
        public readonly static string privateKey = "test";
    }
}
